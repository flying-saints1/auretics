
<?php if(have_rows('join_today_section', 'option')): ?>
    <?php while(have_rows('join_today_section', 'option')): the_row(); ?>
        <section class="profile">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                        <div class="join-today">
                            <h2><?php echo get_sub_field('section_heading'); ?></h2>
                            <p><?php echo get_sub_field('content'); ?></p>
                            <?php
                                $link = get_sub_field('link');
                                if ($link) :
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                else :
                                    $link_url = '#';
                                endif;
                            ?>
                                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="profile-image">
                            <img src="<?php echo get_sub_field('image_desktop'); ?>" class="img-fluid d-none" alt="profile" loading="lazy">
                            <img src="<?php echo get_sub_field('image_mobile'); ?>" class="img-fluid m-block" alt="profile" loading="lazy">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

