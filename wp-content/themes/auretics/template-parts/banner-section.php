
<?php if (have_rows('background_image')) : ?>
    <?php while (have_rows('background_image')) : the_row(); ?>
        <section class="banner" style="background-image: url('<?php echo get_sub_field('image') ?>');">
            <div class="container">
            <div class="row">
            <div class="heading-business">
                    <h1><?php echo get_sub_field('title'); ?></h1>
                    <p><?php echo get_sub_field('para'); ?></p>
                </div>
            </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>
