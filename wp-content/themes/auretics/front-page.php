<?php
/*
 Template Name: Home Page Template
  */
get_header();
?>

<?php if (have_rows('banner_section')) : ?>
  <?php while (have_rows('banner_section')) : the_row(); ?>
    <section class="banner home-banner">
      <div class="blank_div"></div>
      <div class="container">
        <video playsinline="" id="my-video" width="100%" height="100%" loop="" muted="" autoplay="autoplay" loading="lazy">
          <source src="<?php echo get_sub_field('video'); ?>" type="video/mp4">
        </video>
        <div class="row">
          <div class="col-md-8 col-8 col-sm-8 banner-video-text">
            <div class="heading-business home-business">
              <h1><?php echo get_sub_field('heading'); ?></h1>
              <p><?php echo get_sub_field('text'); ?></p>
              <?php
              $link = get_sub_field('link');
              if ($link) :
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-md-4 col-4 col-sm-4 banner-video-text">
            <div class="banner-logo">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/banner-logo.svg" alt="site-logo">
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('marque_logo_brand')) : ?>
  <section class="marquee-line">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="marque-slider">
            <?php while (have_rows('marque_logo_brand')) : the_row(); ?>
              <div>
                <ul>
                  <li>
                    <span>
                      <img src="<?php echo get_sub_field('image') ?>" alt="" width="56px" height="56px"<?php echo get_sub_field('text'); ?>">
                    </span>
                    <?php echo get_sub_field('text'); ?>
                  </li>
                </ul>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>


<?php if (have_rows('growth_section')) : ?>
  <?php while (have_rows('growth_section')) : the_row(); ?>
    <section class="growth">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="row">
              <div class="col-12">
                <div class="growth-heading">
                  <p class="orange-color"><?php echo get_sub_field('title') ?></p>
                  <h2 class="pb-4"><?php echo get_sub_field('heading') ?></h2>
                </div>
              </div>
            </div>
            <?php if (have_rows('content_box')) : ?>
              <?php while (have_rows('content_box')) : the_row(); ?>
                <div class="row no-gutter background-color">
                  <div class="col-md-6 boss">
                    <img src="<?php echo get_sub_field('image') ?>" alt="growth" class="img-fluid">
                  </div>
                  <div class="col-md-6">
                    <div class="boss-content">
                      <h3><?php echo get_sub_field('content_box_heading') ?></h3>
                      <p><?php echo get_sub_field('content') ?></p>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('about_auretics')) : ?>
  <?php while (have_rows('about_auretics')) : the_row(); ?>
    <section class="auretics-limited" style="background-image: url('<?php echo get_sub_field('background_image') ?>');">
      <div class="container">
        <div class="row">
          <?php if (have_rows('left_side_content')) : ?>
            <?php while (have_rows('left_side_content')) : the_row(); ?>
              <div class="col-md-12 col-lg-12 col-xl-6">
                <div class="limited-wrapper">
                  <img src="<?php echo get_sub_field('about_image') ?>" alt="limited-background" class="img-fluid" loading="lazy" width="648px" height="607px";>
                  <div class="fastest d-flex">
                    <div> <img src="<?php echo get_sub_field('icon_image') ?>" alt="ribbon" width="72px" height="72px"></div>
                    <div class="fast-content">
                      <h3><?php echo get_sub_field('heading') ?></h3>
                      <p><?php echo get_sub_field('text') ?></p>
                    </div>
                  </div>
                </div>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>

          <?php if (have_rows('right_side_content')) : ?>
            <?php while (have_rows('right_side_content')) : the_row(); ?>
              <div class="col-md-12 col-lg-12 col-xl-6">
                <div class="right-fast-content">
                  <h2 class="pb-3"><?php echo get_sub_field('about_us_heading') ?></h2>
                  <p><?php echo get_sub_field('content') ?></p>
                  <?php
                  $link = get_sub_field('link');
                  if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                  ?>
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
                  <?php endif; ?>
                </div>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('product_category_section')) : ?>
  <?php while (have_rows('product_category_section')) : the_row(); ?>
    <section class="product-categories">
      <div class="container">
        <div class="row">
          <div class="range">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>

          <div class="discover-category-slider">
            <?php if (have_rows('category_box')) : ?>
              <?php while (have_rows('category_box')) : the_row(); ?>
                <div class="col-md-3">
                  <div class="discover-category">
                    <img src="<?php echo get_sub_field('category_image') ?>" alt="category" class="img-fluid" loading="lazy">
                    <h6><?php echo get_sub_field('category_title') ?></h6>
                    <p><?php echo get_sub_field('category_content') ?></p>
                    <?php
                    $link = get_sub_field('link');
                    if ($link) :
                      $link_url = $link['url'];
                      $link_title = $link['title'];
                      $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                      <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
                    <?php
                    endif;
                    ?>
                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('products_tab')) : ?>
  <?php while (have_rows('products_tab')) : the_row(); ?>
    <section class="products">
      <div class="container">
        <div class="row">
          <div class="range-2">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="product-slider">

            <?php if (have_rows('products')) : ?>
              <?php while (have_rows('products')) : the_row(); ?>
                <div class="col-md-3 product-border">
                  <?php
                  $link = get_sub_field('link');
                  if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                  ?>
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>">
                    <?php
                  endif;
                    ?>
                    <img src="<?php echo get_sub_field('product_image'); ?>" alt="<?php echo get_sub_field('product_title'); ?>" loading="lazy" width="292px" height="301px";>
                    <div class="products-data text-center">
                      <?php if ($rating = get_sub_field('rating')) : ?>
                        <div class="rating-icons">
                          <?php for ($i = 1; $i <= $rating; $i++) : ?>
                            <i class="fa-solid fa-star"></i>
                          <?php endfor; ?>
                        </div>
                      <?php endif; ?>
                      <span><?php echo get_sub_field('product_title') ?></span>
                      <p class="p-0 m-0"><?php echo get_sub_field('product_price_before') ?></p>
                      <p><?php echo get_sub_field('product_price') ?></p>
                    
                    </div>
                    </a>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('certification_section')) : ?>
  <?php while (have_rows('certification_section')) : the_row(); ?>
    <section class="certification">
      <div class="container">
        <div class="row">
          <div class="range-3 pb-3">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-3"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <?php if (have_rows('iso_images')) : ?>
            <?php $i = 1; ?>
            <?php while (have_rows('iso_images')) : the_row(); ?>
              <div class="col-md-6 col-lg-4">
                <div class="certificate-image">
                  <img src="<?php echo get_sub_field('image') ?>" alt="certificate -<?php echo $i; ?>" loading="lazy" width="403px" height="320px">
                </div>
              </div>
              <?php $i++; ?>
            <?php endwhile; ?>
          <?php endif; ?>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('rewards_recognition')) : ?>
  <?php while (have_rows('rewards_recognition')) : the_row(); ?>
    <section class="product-categories">
      <div class="container">
        <div class="row">
          <div class="range">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="category-slider">
            <?php if (have_rows('rewards_box')) : ?>
              <?php while (have_rows('rewards_box')) : the_row(); ?>
                <div class="col-md-4">
                  <div class="discover-category">
                    <img src="<?php echo get_sub_field('rewards_image') ?>" alt="category" loading="lazy">
                    <h6><?php echo get_sub_field('rewards_title') ?></h6>
                    <p><?php echo get_sub_field('rewards_content') ?></p>
                    <?php
                    $link = get_sub_field('link');
                    if ($link) :
                      $link_url = $link['url'];
                      $link_title = $link['title'];
                      $link_target = $link['target'] ? $link['target'] : '_self';
                    else :
                      $link_url = '#';
                    endif;
                    ?>
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('testimonil_section_home')) : ?>
  <?php while (have_rows('testimonil_section_home')) : the_row(); ?>
    <section class="testimonial">
      <div class="container">
        <div class="row">
          <div class="testi-one">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="home-testimonial-slider">
            <?php if (have_rows('testimonial')) : ?>
              <?php $i = 1; ?>
              <?php while (have_rows('testimonial')) : the_row(); ?>
                <div class="col-md-4">
                  <div class="testimonial-one">
                    <a href="#test-video<?php echo $i; ?>" class="testimonial-popup">
                      <div class="img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg" alt="arrow">
                        <h3><span><?php echo get_sub_field('heading') ?></span></h3>
                        <p class="join-para"><?php echo get_sub_field('review') ?></p>
                        <div class="author">
                          <p>
                            <span><?php echo get_sub_field('reviewer_name') ?></span>

                            <span><?php echo get_sub_field('reviewer__title') ?></span>
                          </p>
                          <span>
                            <img src="<?php echo get_sub_field('image') ?>" class="img-fluid" alt="Mr. Vikas Kumar Dey" loading="lazy">
                          </span>
                        </div>
                      </div>
                    </a>

                  </div>
                </div>
                <?php $i++; ?>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <?php
          $link = get_sub_field('link');
          if ($link) :
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
          ?>
            <div class="test-btn">
              <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
            </div>
          <?php

          endif;
          ?>

        </div>
        <?php if (have_rows('testimonial')) : ?>
          <?php $i = 1; ?>
          <?php while (have_rows('testimonial')) : the_row(); ?>
            <div class="mfp-hide testimonial-popup-video" id="test-video<?php echo $i; ?>">
              <iframe width="560" height="315" src="<?php echo get_sub_field('youtube_video_embed_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
              <button title="Close (Esc)" type="button" class="mfp-close">×</button>
            </div>
            <?php $i++; ?>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('8_things_know')) : ?>
  <?php while (have_rows('8_things_know')) : the_row(); ?>
    <section class="eight-things" style="background-image: url('<?php echo get_sub_field('background_image') ?>');">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="eight-things-wrapper">
              <p class="white-wrap"><?php echo get_sub_field('title') ?></p>
              <h2 class="white-wrap"><?php echo get_sub_field('heading') ?></h2>
            </div>
          </div>
        </div>
        <?php if (have_rows('steps')) : ?>
          <?php $i = 1; ?>
          <div class="row">
            <div class="col-12">
              <div class="row mt-5 desktop-content">
                <div class="col-12">
                  <div class="ct-image-box text-center">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/group-circle.png" alt="things" loading="lazy">
                    <?php while (have_rows('steps')) : the_row(); ?>
                      <div class="ct-list-item" id="number<?php echo $i; ?>">
                        <div class="ct-list-number"><?php if ($i < 10) :  echo '0' . $i;
                                                    else : echo $i;
                                                    endif; ?></div>
                        <div class="ct-list-content"><?php echo get_sub_field('steps_title') ?></div>
                      </div>
                      <?php $i++; ?>
                    <?php endwhile; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <?php if (have_rows('steps')) : ?>
          <?php $i = 1; ?>
          <div class="row mobile-space">
            <div class="mob-thing-img">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/group-circle.png" alt="things" loading="lazy">
            </div>
            <?php while (have_rows('steps')) : the_row(); ?>
              <?php if ($i == 1 || $i == 5) : ?><div class="col-md-6 mobile-content <?php if ($i == 5) : ?>  <?php endif; ?>"><?php endif; ?>
                <div class="ct-list-item <?php if ($i > 4) : ?> list-side <?php endif; ?>">
                  <div class="ct-list-number"><?php if ($i < 10) :  echo '0' . $i;
                                              else : echo $i;
                                              endif; ?></div>
                  <div class="ct-list-content"> <?php echo get_sub_field('steps_title') ?></div>
                </div>
                <?php if ($i == 4 || $i == 8) : ?>
                </div><?php endif; ?>
              <?php $i++; ?>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('captivating_section')) : ?>
  <?php while (have_rows('captivating_section')) : the_row(); ?>
    <section class="capitative">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <div class="everywhere">
              <h2><?php echo get_sub_field('heading'); ?></h2>
            </div>
          </div>
          <div class="col-md-5">
            <div class="grabbed">
              <p><?php echo get_sub_field('text'); ?></p>
            </div>
          </div>
        </div>
        <?php if (have_rows('captivates')) : ?>
          <div class="row stories">
            <?php $i = 1; ?>
            <?php while (have_rows('captivates')) : the_row(); ?>
              <?php if ($i == 1 || $i == 4) : ?><div class="col-md-3 col-12"><?php endif; ?>
                <?php if ($i == 3) : ?><div class="col-md-6 col-12 d-flex align-items-center"><?php endif; ?>
                  <?php
                  $link = get_sub_field('link');
                  if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                  else :
                    $link_url = "#";
                  endif;
                  ?>
                  <div class="image-box">
                    <a href="<?php echo $link_url ?>" class="award-video">
                      <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid" alt="audience" loading="lazy">
                    </a>
                    <p><?php echo get_sub_field('title'); ?></p>
                    <?php if ($link) :  ?>
                      <div class="explore-button">
                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                      </div>
                    <?php endif; ?>
                  </div>
                  <?php if ($i == 2 || $i == 5 || $i == 3) : ?>
                  </div><?php endif; ?>
                <?php $i++; ?>
              <?php endwhile; ?>
                </div>
              <?php endif; ?>
          </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('coverage_media')) : ?>
  <?php while (have_rows('coverage_media')) : the_row(); ?>
    <section class="news-press">
      <div class="container">
        <div class="row">
          <div class="press">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="row justify-content-center align-items-center">
            <?php if (have_rows('news_image')) : ?>
              <?php while (have_rows('news_image')) : the_row(); ?>
                <div class="col-md-2 col-6">
                  <img src="<?php echo get_sub_field('image') ?>" class="img-fluid" alt="audience" loading="lazy" width="180px" height="80px";>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php
// Banner Section
get_template_part('template-parts/join-today-section');
?>


<?php
// Banner Section
get_template_part('template-parts/contact-form');
?>






<?php
get_footer();
?>