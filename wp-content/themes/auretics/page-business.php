<?php
/*
 Template Name: Business  Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php if (have_rows('introduction_section')) : ?>
    <?php while (have_rows('introduction_section')) : the_row(); ?>
        <section class="opportunity">
            <div class="container">
                <div class="row">
                    <div class="text-center">
                        <p class="explore"><?php echo get_sub_field('title'); ?></p>
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                        <p class="para"><?php echo get_sub_field('content'); ?></p>
                        <?php
                        $link = get_sub_field('link');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        else :
                            $link_url = '#';
                        endif;
                        ?>
                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" title="" class="read-btn"> <button><?php echo esc_attr($link_title); ?></button> </a>
                        <!-- <a href=""></a> -->
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('ways_of_income_section')) : ?>
    <?php while (have_rows('ways_of_income_section')) : the_row(); ?>
        <section class="compensation" style="background-image: url('<?php echo get_sub_field('background_image'); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="all-compensation">
                            <p class="explore"><?php echo get_sub_field('title'); ?></p>
                            <h2 style="color:#FFf;"><?php echo get_sub_field('heading'); ?></h2>
                        </div>
                    </div>
                    <div class="col-md-7 content-box">
                        <?php echo get_sub_field('content'); ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('steps_section')) : ?>
    <?php while (have_rows('steps_section')) : the_row(); ?>
        <section class="sucessful">
            <div class="container">
                <div class="success-data text-center">
                    <p class="explore"><?php echo get_sub_field('title'); ?></p>
                    <h2><?php echo get_sub_field('heading'); ?></h2>
                </div>
                <div class="row">
                    <?php if (have_rows('steps')) : ?>
                        <?php while (have_rows('steps')) : the_row(); ?>
                            <div class="col-md-6 steps-box">
                                <?php
                                $link = get_sub_field('link');
                                if ($link) :
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                else :
                                    $link_url = '#';
                                endif;
                                ?>
                                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><button class="button mb-4"><?php echo esc_attr($link_title); ?></button></a>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>



<?php if (have_rows('explore_with_us_section')) : ?>
    <?php while (have_rows('explore_with_us_section')) : the_row(); ?>
        <section class="opportunity-option" style="background-image: url('<?php echo get_sub_field('background_image'); ?>');">
            <div class="container">
                <div class="text-center">
                    <p class="explore"><?php echo get_sub_field('title'); ?></p>
                    <h2><?php echo get_sub_field('heading'); ?></h2>
                    <p class="pb-3"><?php echo get_sub_field('content'); ?></p>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="opportunity-slider">
                            <?php if (have_rows('options_box')) : ?>
                                <?php while (have_rows('options_box')) : the_row(); ?>

                                    <div class="double-box">
                                        <div class="icon-box text-center">
                                            <img src="<?php echo get_sub_field('image'); ?>" alt="tbb" class="img-fluid">
                                        </div>
                                        <div class="four-data-box text-center">
                                            <h3><?php echo get_sub_field('head'); ?></h3>
                                            <p><?php echo get_sub_field('paragraph'); ?></p>
                                        </div>
                                    </div>


                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php endwhile; ?>
<?php endif;  ?>


<?php if (have_rows('apka_apna_business')) : ?>
    <?php while (have_rows('apka_apna_business')) : the_row(); ?>
        <section class="Auretics-apna business">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="text-center">
                            <h2><?php echo get_sub_field('heading'); ?></h2>
                            <p><?php echo get_sub_field('intro_content'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php $plan = get_sub_field('plan'); ?>
            <?php if (have_rows('plan')) : ?>
                <div class="container apna-icon-area pt-5">
                    <?php $i = 1; ?>
                    <?php $points = 5; ?>
                    <?php $nextline = 5; ?>
                    <?php $iteration = 1; ?>
                    <?php $count = count($plan); ?>
                    <?php while (have_rows('plan')) : the_row(); ?>
                        <?php if ($i == 1 || $i == ($nextline + 1)) : ?>
                            <div class="row  <?php if ($iteration % 2 == 0) : ?> justify-content-start flex-row-reverse <?php endif; ?> <?php if ($count > ($points * $iteration) && $count > 5) : ?>border-on <?php endif; ?>">
                                <?php $nextline = $nextline * $iteration;   ?>
                            <?php endif; ?>
                            <div class="apna-business-icon ">
                                <div class="img <?php if ($iteration % 2 == 0 && $i == ($points * $iteration) && $i != $count) : ?> last <?php endif; ?> <?php if ($iteration % 2 != 0 && $i == $count && $i > 10) : ?> last <?php endif; ?>">
                                    <div class="image-inner ">
                                        <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid" loading="lazy" alt="<?php echo get_sub_field('heading'); ?>">
                                    </div>
                                    <h6><?php echo get_sub_field('heading'); ?></h6>
                                    <p><?php echo get_sub_field('text'); ?></p>
                                </div>
                            </div>

                            <?php if ($i == 5 || $i == 10 || $i == $count) : ?>
                            </div> <?php $iteration++;
                                endif; ?>
                        <?php $i++; ?>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </section>
    <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('apka_apna_business')) : ?>
    <?php while (have_rows('apka_apna_business')) : the_row(); ?>
        <!-- Mobile -->
        <section class="Auretics-apna business mob-apna-business">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="text-center">
                            <h2><?php echo get_sub_field('heading'); ?></h2>
                            <p class="pb-5"><?php echo get_sub_field('intro_content'); ?></p>
                        </div>
                    </div>
                </div>

                <?php if (have_rows('plan')) : ?>
                    <div class="row justify-content-center text-center">
                        <?php while (have_rows('plan')) : the_row(); ?>
                            <div class="col-6">
                                <div class="img-time">
                                    <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid" loading="lazy" alt="<?php echo get_sub_field('heading'); ?>">
                                    <h6><?php echo get_sub_field('heading'); ?></h6>
                                    <p><?php echo get_sub_field('text'); ?></p>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('more_rewards_section')) : ?>
    <?php while (have_rows('more_rewards_section')) : the_row(); ?>
        <section class="more-rewards">
            <div class="container">
                <div class="row justify-content-center text-center">
                    <div class="col-12">
                        <h2 class="pb-5"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                    <?php if (have_rows('rewards')) : ?>
                        <?php while (have_rows('rewards')) : the_row(); ?>
                            <div class="col-3 col-md-6 col-lg-3">
                                <img src="<?php echo get_sub_field('image'); ?>" class="img-fluid" loading="lazy" alt="<?php echo get_sub_field('heading'); ?>">
                                <h6><?php echo get_sub_field('heading'); ?></h6>
                                <p><?php echo get_sub_field('text'); ?></p>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('lifestyle_rewards_section')) : ?>
    <?php while (have_rows('lifestyle_rewards_section')) : the_row(); ?>
        <section class="organization pt-0 mt-5 procedure management-team">
            <div class="container">
                <div class="row text-center pb-5">
                    <div class="col-md-12">
                        <h2 class="pt-2"><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                </div>

                <?php if (have_rows('team_box')) : ?>
                    <div class="row on-desktop justify-content-center">
                        <?php while (have_rows('team_box')) : the_row(); ?>
                            <div class="col-md-3">
                                <div class="organize-img">
                                    <img src="<?php echo get_sub_field('image'); ?>" alt="T-shirt" loading="lazy">
                                </div>
                                <div class="wrapper-team">
                                    <div class="organize-wrap">
                                        <h6><?php echo get_sub_field('name'); ?></h6>
                                        <p><?php echo get_sub_field('title'); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

                <?php if (have_rows('team_box')) : ?>
                    <div class="row pt-0 on-mobile justify-content-center">
                        <div class="lifestyle-slider">
                            <?php while (have_rows('team_box')) : the_row(); ?>
                                <div class="col-md-3">
                                    <div class="organize-img">
                                        <img src="<?php echo get_sub_field('image'); ?>" alt="T-shirt" loading="lazy">
                                    </div>
                                    <div class="wrapper-team">
                                        <div class="organize-wrap">
                                        <h6><?php echo get_sub_field('name'); ?></h6>
                                        <p><?php echo get_sub_field('title'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('apka_apna_business')) : ?>
    <?php while (have_rows('apka_apna_business')) : the_row(); ?>
        <section class=" business-class">
            <div class="container">
                <div class="row">
                    <p><?php echo get_sub_field('content'); ?></p>

                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>



<?php
get_footer();
?>