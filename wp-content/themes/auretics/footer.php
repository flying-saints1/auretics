<footer class="section-footer footer-dark" style="background-image: url('<?php echo get_field('background__image', 'option'); ?>">
    <div class="container">
        <div class="row footer-main p-y copyright">
            <div class="col-md-3 mobile-block">
                <a href="<?php echo get_site_url(); ?>">
                    <img src="<?php echo get_field('footer_logo', 'option'); ?>" alt="copyright" class="img-fluid footer-logo" loading="lazy">
                </a>
                <p class="follow">Follow us on</p>
                <?php $social_links = get_field('social_links', 'option'); ?>
                <?php if ($social_links) : ?>
                    <?php foreach ($social_links as $social) : ?>
                        <a href="<?php echo $social['link']; ?>">
                            <i class="fa-brands <?php echo $social['class_svg']; ?>"></i>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
            <aside class="col-12 col-sm-12 col-lg-4 display-block">
                <h6 class="auretics">AURETICS LIMITED</h6>
                <?php if (have_rows('address', 'option')) : ?>
                    <?php while (have_rows('address', 'option')) : the_row(); ?>
                        <article class="me-lg-4">
                            <p class="mt-3"><strong> <?php echo get_sub_field('address_heading'); ?></strong></p>
                            <p> <?php echo get_sub_field('location_or_email'); ?></p>
                        </article>
                    <?php endwhile; ?>
                <?php endif; ?>
            </aside>
            <aside class="col-6 col-sm-4 col-lg-2">
                <h3 class="title pt-0 useful-style">BUSINESS TOOLS</h3>

                <?php
                // Header Menu
                if (function_exists('register_primary_menu')) :
                    wp_nav_menu(array(
                        'theme_location' => 'Links',
                        'menu_class' => 'list-menu',
                        'container_id' => '',
                        'container_class' => '',
                        // 'add_li_class'  => 'nav-item',
                        // 'add_a_class'  => 'nav-link',
                    ));
                endif;
                ?>
            </aside>
            <aside class="col-6 col-sm-4 col-lg-2">
                <h3 class="title pt-0 useful-style">USEFUL LINKS</h3>

                <?php
                // Header Menu
                if (function_exists('register_primary_menu')) :
                    wp_nav_menu(array(
                        'theme_location' => 'Links2',
                        'menu_class' => 'list-menu',
                        'container_id' => '',
                        'container_class' => '',
                        // 'add_li_class'  => 'nav-item',
                        // 'add_a_class'  => 'nav-link',
                    ));
                endif;
                ?>
            </aside>
            <aside class="col-6 col-sm-4 col-lg-2">
                <h3 class="title pt-0 useful-style">INFORMATION</h3>

                <?php
                // Header Menu
                if (function_exists('register_primary_menu')) :
                    wp_nav_menu(array(
                        'theme_location' => 'Links3',
                        'menu_class' => 'list-menu',
                        'container_id' => '',
                        'container_class' => '',
                        // 'add_li_class'  => 'nav-item',
                        // 'add_a_class'  => 'nav-link',
                    ));
                endif;
                ?>
            </aside>

            <aside class="col-6 col-sm-12 col-lg-2">
                <h3 class="title">PRODUCTS</h3>

                <?php
                // Header Menu
                if (function_exists('register_primary_menu')) :
                    wp_nav_menu(array(
                        'theme_location' => 'Links4',
                        'menu_class' => 'list-menu',
                        'container_id' => '',
                        'container_class' => '',
                        // 'add_li_class'  => 'nav-item',
                        // 'add_a_class'  => 'nav-link',
                    ));
                endif;
                ?>
            </aside>

            <aside class="col-12 col-sm-12 col-lg-4 mobile-block mobile-margin">
                <h6 class="auretics">Auretics Limited</h6>
                <?php if (have_rows('address', 'option')) : ?>
                    <?php while (have_rows('address', 'option')) : the_row(); ?>
                        <article class="me-lg-4">
                            <p class="mt-3"><strong> <?php echo get_sub_field('address_heading'); ?></strong></p>
                            <p> <?php echo get_sub_field('location_or_email'); ?></p>
                        </article>
                    <?php endwhile; ?>
                <?php endif; ?>
            </aside>
        </div>
        <div class="row copyright">

            <div class="col-md-2 display-block">

                <a href="<?php echo get_site_url(); ?>">
                    <img src="<?php echo get_field('footer_logo', 'option'); ?>" alt="copyright" class="img-fluid footer-logo" loading="lazy">
                </a>
                <p class="follow">Follow us on</p>
                <?php $social_links = get_field('social_links', 'option'); ?>
                <?php if ($social_links) : ?>
                    <?php foreach ($social_links as $social) : ?>
                        <a href="<?php echo $social['link']; ?>">
                            <i class="fa-brands <?php echo $social['class_svg']; ?>"></i>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>
            <div class="col-md-10">
                <p class="grivence"><?php echo get_field('copyright', 'option') ?></p>
                <p class="limited"><?php echo get_field('limited', 'option') ?></p>
            </div>
        </div>



    </div>
</footer>


<?php wp_footer(); ?>



<?php if (get_field('map_api_key', 'option') != null) : ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_field('map_api_key', 'option'); ?>"></script>
<?php else : ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6gvzm4-aftiYK68fumTbEc4LXQkEB4A8"></script>
<?php endif; ?>


<script type="text/javascript">
    if (document.querySelectorAll('#map').length > 0) {
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var get_lat, get_long;
            if (document.querySelectorAll('#lat').length > 0 && document.querySelectorAll('#long').length > 0) {
                get_lat = document.getElementById('lat').value;
                get_long = document.getElementById('long').value;
            } else {
                get_lat = 40.6700; //Default Latitude
                get_long = -73.9400; //Default Longitude

            }

            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 14,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(get_lat, get_long), // New York

                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{
                        "featureType": "administrative.land_parcel",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "landscape.man_made",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels",
                        "stylers": [{
                                "visibility": "simplified"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [{
                            "hue": "#f49935"
                        }]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [{
                            "hue": "#fad959"
                        }]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels",
                        "stylers": [{
                            "visibility": "simplified"
                        }]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [{
                            "visibility": "off"
                        }]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [{
                                "hue": "#a1cdfc"
                            },
                            {
                                "saturation": 30
                            },
                            {
                                "lightness": 49
                            }
                        ]
                    }
                ]
            };

            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(get_lat, get_long),
                map: map,
                title: 'Snazzy!'
            });
        }
    }
</script>



</body>

</html>