<?php
/*
 Template Name: 404 Page Template
  */
get_header();
?>

<section class="error-page" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/404-banner-1.jpg');">
    <div class="container">
        <div class="row">
            <div class="404-content text-center">
                <h2 class="page-title">404</h2>
                <p class="page-field">Something went wrong!</p>
                <p class="small-para">"The page you are looking for has been removed, had its name changed or temporarily unavailable."</p>
                <a href="">GO BACK TO HOMEPAGE</a>
            </div>
        </div>
    </div>
</section>
























<?php
get_footer();
?>
