<?php
/*
 Template Name: FAQ Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php if(have_rows('faqs')): ?>
<?php while(have_rows('faqs')): the_row(); ?>
<section class="faq">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="text-center"><?php echo get_sub_field('heading'); ?></h2>
                </div>
            </div>

            <?php if(have_rows('faq_tab')): ?>
                
                <div class="row accordion" id="accordionExample">
                    <?php $i = 1;?>
                    <?php while(have_rows('faq_tab')): the_row(); ?>
                        <div class="col-md-6 mt-3 ">
                            <div class="accordion-item">
                                <h3 class="accordion-header" id="heading<?php echo $i; ?>"> <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse1"><?php echo get_sub_field('question'); ?></button></h3>
                                <div id="collapse<?php echo $i; ?>" class="accordion-collapse collapse" aria-labelledby="heading<?php echo $i; ?>" data-bs-parent="#accordionExample" style="">
                                    <div class="accordion-body"><?php echo get_sub_field('answers'); ?></div>
                                </div>
                            </div>
                        </div>
                    <?php $i++; ?>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>


























<?php
get_footer();
?>