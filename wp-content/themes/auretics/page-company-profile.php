<?php
/*
 Template Name: Profile Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php if (have_rows('about_section')) : ?>
<?php while (have_rows('about_section')) : the_row(); ?>
<section class="auretics-limited">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-4">
                <div class="limited-image">
                    <img src="<?php echo get_sub_field('image'); ?>" alt="About Company" class="img-fluid" loading="lazy">
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-xl-8">
                <div class="about-company-profile">
                    <h2><?php echo get_sub_field('heading'); ?></h2>
                    <p><?php echo get_sub_field('text'); ?></p>
                </div>

            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('why_auretics')) : ?>
<?php while (have_rows('why_auretics')) : the_row(); ?>
<section class="auretics-carrer-build">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center"><?php echo get_sub_field('heading'); ?></h3>
            </div>
        </div>
        <?php if (have_rows('reasons')) : ?>
        <div class="row auretics-slider">
            <?php while (have_rows('reasons')) : the_row(); ?>
            <div class="col-lg-3 col-md-4 ">
                <div class="user-icon">
                    <img src="<?php echo get_sub_field('icon') ; ?>" alt="" loading="lazy">
                    <h6><?php echo get_sub_field('heading') ; ?></h6>
                    <p><?php echo get_sub_field('text') ; ?></p>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('auretics_points')) : ?>
<?php while (have_rows('auretics_points')) : the_row(); ?>
<section class="auretics-list-items">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="row">
                    <div class="heading-auretics-list pb-4">
                        <h3><?php echo get_sub_field('heading'); ?></h3>
                    </div>
                    <?php if (have_rows('points')) : ?>
                    <?php while (have_rows('points')) : the_row(); ?>
                    <div class="col-md-6">
                        <div class="assurance-list">
                            <h4><?php echo get_sub_field('heading'); ?></h4>
                            <p><?php echo get_sub_field('text'); ?></p>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('mission_vision')) : ?>
<?php while (have_rows('mission_vision')) : the_row(); ?>
<section class="auretics-excellence d-none d-md-block" style="background-image: url('<?php echo get_sub_field('desktop_background'); ?>');">
    <div class="container">
        <?php if (have_rows('Tab')) : ?>
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4">
                        <div class="mission-all">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <?php $i=1; ?>
                                <?php while (have_rows('Tab')) : the_row(); ?>
                                    <button class="nav-link <?php if($i==1): ?>active <?php endif; ?>" id="nav-home-tab<?php echo $i; ?>" data-bs-toggle="tab" data-bs-target="#nav-home<?php echo $i; ?>" type="button" role="tab" aria-controls="nav-home" aria-selected="<?php if($i==1): ?>true <?php else: ?> false <?php endif; ?>">
                                        <h4><?php echo get_sub_field('tab_title'); ?></h4>
                                    </button>
                                <?php $i++; ?>
                                <?php endwhile; ?>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content" id="nav-tabContent">
                            <?php $i=1; ?>
                            <?php while (have_rows('Tab')) : the_row(); ?>
                            <div class="tab-pane fade  <?php if($i==1): ?> show active <?php endif; ?>" id="nav-home<?php echo $i; ?>" role="tabpanel" aria-labelledby="nav-home-tab<?php echo $i; ?>">
                                <h5><?php echo get_sub_field('tab_heading'); ?></h5>
                                <?php echo get_sub_field('tab_content'); ?>
                            </div>
                            <?php $i++; ?>
                            <?php endwhile; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('mission_vision')) : ?>
<?php while (have_rows('mission_vision')) : the_row(); ?>
<section class="mission-values d-block d-md-none">
    <div class="container">
        
        <div class="row">
            <div class="heading-mision-values">
                <h2 class="text-center"><?php echo get_sub_field('heading'); ?></h2>
            </div>
            <?php if (have_rows('Tab')) : ?>
            <div class="our-mission-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <?php $i=3; ?>
                        <?php while (have_rows('Tab')) : the_row(); ?>
                            <button class="nav-link <?php if($i==3): ?>active <?php endif; ?>" id="nav-home-tab<?php echo $i; ?>" data-bs-toggle="tab" data-bs-target="#nav-home<?php echo $i; ?>" type="button" role="tab" aria-controls="nav-home" aria-selected="<?php if($i==1): ?>true <?php else: ?> false <?php endif; ?>">
                                <h4><?php echo get_sub_field('tab_title'); ?></h4>
                            </button>
                        <?php $i++; ?>
                        <?php endwhile; ?>
                    </div>
                </nav>
            </div>
            <div class="vision-tab" style="background-image: url('<?php echo get_sub_field('mobile_background'); ?>');">
                <div class="tab-content" id="nav-tabContent">
                    <?php $i=3; ?>
                    <?php while (have_rows('Tab')) : the_row(); ?>
                    <div class="tab-pane fade <?php if($i==3): ?> show active <?php endif; ?>" id="nav-home<?php echo $i; ?>" role="tabpanel" aria-labelledby="nav-home-tab<?php echo $i; ?>">
                        <h5><?php echo get_sub_field('tab_heading'); ?></h5>
                        <h5><?php echo get_sub_field('tab_content'); ?></h5>
                    </div>
                    <?php $i++; ?>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
</section>
<?php endwhile; ?>
<?php endif; ?>



<?php if (have_rows('who_we_are')) : ?>
<?php while (have_rows('who_we_are')) : the_row(); ?>
<section class="Who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="spreading">
                    <p class="spread-not d-md-block d-none"><?php echo get_sub_field('title'); ?></p>
                    <h2 class=" d-md-block d-none"><?php echo get_sub_field('heading'); ?></h2>
                    <p><?php echo get_sub_field('text'); ?></p>
                    <?php if (have_rows('points')) : ?>
                    <?php while (have_rows('points')) : the_row(); ?>
                    <p>
                        <span>
                            <svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.58075 13.7538L15.3038 7.03075L14.25 5.97693L8.58075 11.6462L5.73075 8.79615L4.67693 9.84998L8.58075 13.7538ZM10.0016 19C8.68772 19 7.45268 18.7506 6.29655 18.252C5.1404 17.7533 4.13472 17.0765 3.2795 16.2217C2.42427 15.3669 1.74721 14.3616 1.24833 13.206C0.749442 12.0504 0.5 10.8156 0.5 9.50165C0.5 8.18772 0.749334 6.95268 1.248 5.79655C1.74667 4.6404 2.42342 3.63472 3.27825 2.7795C4.1331 1.92427 5.13834 1.24721 6.29398 0.748325C7.44959 0.249442 8.68437 0 9.9983 0C11.3122 0 12.5473 0.249333 13.7034 0.748C14.8596 1.24667 15.8652 1.92342 16.7205 2.77825C17.5757 3.6331 18.2527 4.63834 18.7516 5.79398C19.2505 6.94959 19.5 8.18437 19.5 9.4983C19.5 10.8122 19.2506 12.0473 18.752 13.2034C18.2533 14.3596 17.5765 15.3652 16.7217 16.2205C15.8669 17.0757 14.8616 17.7527 13.706 18.2516C12.5504 18.7505 11.3156 19 10.0016 19ZM9.99998 17.5C12.2333 17.5 14.125 16.725 15.675 15.175C17.225 13.625 18 11.7333 18 9.49998C18 7.26664 17.225 5.37498 15.675 3.82498C14.125 2.27498 12.2333 1.49997 9.99998 1.49997C7.76664 1.49997 5.87498 2.27498 4.32498 3.82498C2.77498 5.37498 1.99998 7.26664 1.99998 9.49998C1.99998 11.7333 2.77498 13.625 4.32498 15.175C5.87498 16.725 7.76664 17.5 9.99998 17.5Z" fill="#FFA41B" />
                            </svg>
                        </span>
                        <?php echo get_sub_field('text'); ?>
                    </p>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    
                </div>

            </div>
            <div class="col-md-12 col-lg-4">
                <div class="finger-block">
                    <p class="spread-not d-md-none d-block"><?php echo get_sub_field('title'); ?></p>
                    <h2 class=" d-md-none d-block"><?php echo get_sub_field('heading'); ?></h2>
                    <img src="<?php echo get_sub_field('image'); ?>" alt="finger-block" class="img-fluid" loading="lazy">
                </div>
            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('empower_section')) : ?>
<?php while (have_rows('empower_section')) : the_row(); ?>
<section class="Empower-people">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="financial-freedom pb-3">
                    <p class="spread-not"><?php echo get_sub_field('title'); ?></p>
                    <h2><?php echo get_sub_field('heading'); ?></h2>
                </div>
            </div>
            <div class="col-md-7">
                <div class="para-fundamental">
                    <p class="pb-3"><?php echo get_sub_field('text'); ?></p>
                </div>
            </div>

            <?php if (have_rows('tab')) : ?>
            <div class="row" id="empower-row">
                <div class="col-lg-5">
                    <div class="three-freedom-tabs">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <?php $i=1; ?>
                                <?php while (have_rows('tab')) : the_row(); ?>
                                <button class="nav-link <?php if($i==1): ?> active <?php endif; ?>" id="nav-boost<?php echo $i; ?>-tab" data-bs-toggle="tab" data-bs-target="#nav-boost<?php echo $i; ?>" type="button" role="tab" aria-controls="nav-boost<?php echo $i; ?>" aria-selected="<?php if($i==1): ?> true <?php else: ?> false <?php endif; ?>">
                                    <div class="boost-one">
                                        <div class="boost-orange">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/orange-arrow.png" alt="Company-detail" class="img-fluid">
                                        </div>
                                        <h5><?php echo get_sub_field('title'); ?></h5>
                                        <p><?php echo get_sub_field('text'); ?></p>
                                    </div>
                                </button>
                                <?php $i++;  ?>
                                <?php endwhile; ?>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="tab-content" id="nav-tabContent">
                        <?php $i=1; ?>
                        <?php while (have_rows('tab')) : the_row(); ?>
                            <div class="tab-pane fade <?php if($i==1): ?> show active <?php endif; ?>" id="nav-boost<?php echo $i; ?>" role="tabpanel" aria-labelledby="nav-boost<?php echo $i; ?>-tab">
                                <img src="<?php echo get_sub_field('desktop_image'); ?>" alt="office" class="img-fluid" loading="lazy">
                                <img src="<?php echo get_sub_field('mobile_image'); ?>" alt="office" class="img-fluid" loading="lazy">
                            </div>
                        <?php $i++;  ?>
                        <?php endwhile; ?>
                    </div>

                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>

</section>
<?php endwhile; ?>
<?php endif; ?>





























<?php
get_footer();
?>