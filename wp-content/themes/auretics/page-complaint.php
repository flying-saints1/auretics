<?php
/*
 Template Name: Complaint  Page Template
  */
get_header();
?>


<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>



<?php if (have_rows('introduction')) : ?>
    <?php while (have_rows('introduction')) : the_row(); ?>
        <section class="procedure">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                    </div>
                    <div class="col-md-8">
                        <p><?php echo get_sub_field('content'); ?></p>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('document_section')) : ?>
    <?php while (have_rows('document_section')) : the_row(); ?>
        <section class="buttons-pillar">    
            <?php if(have_rows('document_tabs')): ?>
            <div class="container text-center">
                <ul class="nav nav-pills mb-5 justify-content-center" id="pills-tab" role="tablist">
                    <?php $i = 1; ?>
                    <?php while (have_rows('document_tabs')) : the_row(); ?>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link <?php if($i == 1): ?> active <?php endif; ?>" id="pills-home-tab-<?php echo $i ;?>" data-bs-toggle="pill" data-bs-target="#pills-home-<?php echo $i ; ?>" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><?php echo get_sub_field('document_tab_heading'); ?></button>
                    </li>
                    <?php $i++; ?>
                    <?php endwhile; ?>
                
                </ul>
                <div class="tab-content" id="pills-tabContent">
                <?php $i= 1; ?>
                <?php while (have_rows('document_tabs')) : the_row(); ?>
                    <div class="tab-pane fade <?php if($i == 1): ?> show active <?php endif; ?>" id="pills-home-<?php echo $i;?>" role="tabpanel" aria-labelledby="pills-home-tab-<?php echo $i;?>">
                        <div class="container">
                            <div class="row"><?php if (have_rows('document_tabs_image')) : ?>
                                <?php while (have_rows('document_tabs_image')) : the_row(); ?>
                                <div class="col-md-4">
                                   <a href="<?php echo get_sub_field('link'); ?>"> <img src="<?php echo get_sub_field('image'); ?>" alt="chart" class="img-fluid" loading="lazy"></a>
                                </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php $i++; ?>
                <?php endwhile; ?>
                
                </div>
            </div>
            <?php endif; ?>
        </section>
    <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('operating_procedure')) : ?>
    <?php while (have_rows('operating_procedure')) : the_row(); ?>
        <section class="standard">
            <div class="container">
                <div class="all-data text-center">
                    <p class="yellow-title"><?php echo get_sub_field('title'); ?></p>
                    <h2><?php echo get_sub_field('heading'); ?></h2>
                    <p><?php echo get_sub_field('content'); ?></p>
                    <img src="<?php echo get_sub_field('image'); ?>" alt="chart" class="img-fluid" loading="lazy">
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>




<?php

get_footer();
?>