<?php
/*
 Template Name: Testimonial Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php if (have_rows('testimonial_reviews')) : ?>
    <?php while (have_rows('testimonial_reviews')) : the_row(); ?>
        <section class="testimonial-box">
            <div class="container">
                <div class="row">
                    <div class="col-12 col--md-12 col-sm-12 col-lg-12">
                        <div class="testimonial-slider">
                            <?php if (have_rows('testimonial')) : ?>
                                <?php $i = 1; ?>
                                <?php while (have_rows('testimonial')) : the_row(); ?>
                                    <div class="testimonial-one">
                                        <a href="#test-video<?php echo $i; ?>" class="testimonial-popup">
                                            <div class="img">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
                                                <h3><?php echo get_sub_field('comment_heading'); ?></h3>
                                                <p class="join-para"><?php echo get_sub_field('content'); ?></p>
                                                <div class="author">
                                                    <p>
                                                        <span><?php echo get_sub_field('reviewer_name'); ?></span>
                                                        <br>
                                                        <span><?php echo get_sub_field('reviewer_title'); ?></span>
                                                    </p>
                                                    <span>
                                                        <img src="<?php echo get_sub_field('reviewer_image'); ?>" class="img-fluid" loading="lazy" alt="<?php echo get_sub_field('reviewer_name'); ?>">
                                                    </span>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if (have_rows('testimonial')) : ?>
                    <?php $i = 1; ?>
                    <?php while (have_rows('testimonial')) : the_row(); ?>
                        <div class="mfp-hide testimonial-popup-video" id="test-video<?php echo $i; ?>">
                        <iframe width="560" height="315" src="<?php echo get_sub_field('youtube_video_embed_link'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
                        </div>
                        <?php $i++; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

        </section>
    <?php endwhile; ?>
<?php endif; ?>








<?php
// Banner Section
get_template_part('template-parts/join-today-section');
?>




<?php
get_footer();
?>