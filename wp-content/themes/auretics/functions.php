<?php

/**
 * Functions and definitions
 */


// Exit-if accessed directly
if (!defined('ABSPATH'))
    exit;


/* THEME OPTIONS PAGE - HEADER, FOOTER, FAQS, CONSULTATION */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title'     => 'Theme Options',
        'menu_title'    => 'Theme Options',
        'menu_slug'     => 'theme-options',
        'capability'    => 'edit_posts',
        'parent_slug'    => '',
        'position'        => false,
        'icon_url'        => false
    ));

    acf_add_options_sub_page(array(
        'page_title'     => 'Header',
        'menu_title'    => 'Header',
        'menu_slug'     => 'theme-options-header',
        'capability'    => 'edit_posts',
        'parent_slug'    => 'theme-options',
        'position'        => false,
        'icon_url'        => false
    ));

    acf_add_options_sub_page(array(
        'page_title'     => 'Footer',
        'menu_title'    => 'Footer',
        'menu_slug'     => 'theme-options-footer',
        'capability'    => 'edit_posts',
        'parent_slug'    => 'theme-options',
        'position'        => false,
        'icon_url'        => false
    ));

    acf_add_options_sub_page(array(
        'page_title'     => 'Common Sections',
        'menu_title'    => 'Common Sections',
        'menu_slug'     => 'theme-options-common',
        'capability'    => 'edit_posts',
        'parent_slug'    => 'theme-options',
        'position'        => false,
        'icon_url'        => false
    ));
}


/* ADD THEME SCRIPTS AND STYLE FILE */
function add_theme_scripts()
{

    /* Styles */
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap5.min.css', false, '5.0', 'all');
    wp_enqueue_style('slick-style', get_template_directory_uri() . '/assets/css/slick.css', false, '2.3.4', 'all');
    wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css', false, '2.3.4', 'all');
    wp_enqueue_style('magnific-popup-min', get_template_directory_uri() . '/assets/css/magnific-popup.min.css', false, '2.3.4', 'all');
    wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css',false, '2.3.4', 'all' );
    wp_enqueue_style('fontawesome-all', get_template_directory_uri() . '/assets/css/all-min.css', false, '6.4', 'all');
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/assets/css/fontawesome.min.css', false, '5.0', 'all');
    wp_enqueue_style('common-style', get_template_directory_uri() . '/assets/css/common.css', false, '1.0', 'all');
    if (is_page_template('front-page.php')) {
        wp_enqueue_style('home-page', get_template_directory_uri() . '/assets/css/home.css', false, '1.0', 'all');
    }
    if (is_page_template('page-complaint.php')) {
        wp_enqueue_style('complaint-page', get_template_directory_uri() . '/assets/css/complaint.css', false, '1.0', 'all');
    }
    if (is_page_template('page-company.php')) {
        wp_enqueue_style('company-page', get_template_directory_uri() . '/assets/css/company.css', false, '1.0', 'all');
    }
    if (is_page_template('page-business.php')) {
        wp_enqueue_style('business-page', get_template_directory_uri() . '/assets/css/business.css', false, '1.0', 'all');
    }
    if (is_page_template('page-company-profile.php')) {
        wp_enqueue_style('profile-page', get_template_directory_uri() . '/assets/css/company-profile.css', false, '1.0', 'all');
    }
    if (is_page_template('page-team.php')) {
        wp_enqueue_style('team-page', get_template_directory_uri() . '/assets/css/team.css', false, '1.0', 'all');
    }
    if (is_page_template('single.php')) {
        wp_enqueue_style('news-page', get_template_directory_uri() . '/assets/css/news.css', false, '1.0', 'all');
    }

    if (is_page_template('page-faq.php')) {
        wp_enqueue_style('faq-page', get_template_directory_uri() . '/assets/css/faq.css', false, '1.0', 'all');
    }
    if (is_page_template('page-testimonial.php')) {
        wp_enqueue_style('testimonial-page', get_template_directory_uri() . '/assets/css/testimonial.css', false, '1.0', 'all');
    }
    if (is_page_template('page-contact.php')) {
        wp_enqueue_style('contact-page', get_template_directory_uri() . '/assets/css/contact.css', false, '1.0', 'all');
    }
    if (is_page_template('page-404.php')) {
        wp_enqueue_style('404-page', get_template_directory_uri() . '/assets/css/404.css', false, '1.0', 'all');
    }
    wp_enqueue_style('blog-detail-page', get_template_directory_uri() . '/assets/css/blog-detail.css', false, '1.0', 'all');
    wp_enqueue_style('common-responsive', get_template_directory_uri() . '/assets/css/responsive.css', false, '1.0', 'all');




    /* Scripts */

    if (!is_admin()) {
        //Call JQuery
        //wp_deregister_script('jquery');
    }

    wp_enqueue_script('jquery-js', get_template_directory_uri() . '/assets/js/jquery.js', array(), null, true);
    wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js', array(), null, true);
    wp_enqueue_script('magnific-popup-min', get_template_directory_uri() . '/assets/js/magnific-popup.min.js', array(), null, true);
    wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/assets/js/magnific-popup.js', array(), null, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), null, true);
    wp_enqueue_script('font-awesome', get_template_directory_uri() . '/assets/js/fontawesome.min.js', array(), null, true);
    wp_enqueue_script('font-awesome-all', get_template_directory_uri() . '/assets/js/all.min.js', array(), null, true);
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array(), null, true);
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

function mind_defer_scripts($tag, $handle, $src)
{
    $defer = array('jquery', 'bootstrap', 'font-awesome', 'font-awesome-all');

    if (in_array($handle, $defer)) {
        return '<script src="' . $src . '" defer type="text/javascript"></script>' . "\n";
    }

    return $tag;
}
add_filter('script_loader_tag', 'mind_defer_scripts', 10, 3);

add_theme_support('post-thumbnails');

// To allow svg file upload in admin
function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
//  

function webp_upload_mimes($existing_mimes)
{
    // add webp to the list of mime types
    $existing_mimes['webp'] = 'image/webp';
    // return the array back to the function with our added mime type
    return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');
//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path)
{
    if ($result === false) {
        $displayable_image_types = array(IMAGETYPE_WEBP);
        $info = @getimagesize($path);
        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }
    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);


/* REGISTER PRIMARY MENUS */
add_action('after_setup_theme', 'register_primary_menu');
function register_primary_menu()
{
    register_nav_menu('Header', __('Header Menu', 'auretics'));
    register_nav_menu('Links', __('Links Menu', 'auretics'));
    register_nav_menu('Links2', __('Links Menu 2', 'auretics'));
    register_nav_menu('Links3', __('Links Menu 3', 'auretics'));
    register_nav_menu('Links4', __('Links Menu 4', 'auretics'));

    // register_nav_menu('Services', __('Services Menu', 'fusion'));
}



function auretics_init()
{

    register_sidebar(
        array(
            'name'          => esc_html__('Recent Post Sidebar', 'auretics'),
            'id'            => 'recent-post-sidebar',
            'description'   => esc_html__('Add widgets here to appear in details blog page.', 'auretics'),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '',
            'after_title'   => '',
        )
    );
}
add_action('widgets_init', 'auretics_init');
